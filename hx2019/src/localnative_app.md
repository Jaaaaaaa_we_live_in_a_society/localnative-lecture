# localnative app

## video
go over the [videos](https://www.youtube.com/channel/UCO3qFIyK0eSmqvMknsslWRw)

## install

go to [localnative.app](https://localnative.app)

download and install v0.4.0 (for both native host os and VM gnu/linux guest if applicable):
- desktop application
- web extension
	- firefox
	- chrome
	- chromium
	- brave
- ios app
- android app

create some notes, try out search, tag, time range filter, qr codes etc.

## blog
[localnative articles](https://localnative.app/blog) about timeline and release updates
