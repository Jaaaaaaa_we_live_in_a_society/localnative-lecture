# github

Local Native GitHub repo for [Bountysource](https://www.bountysource.com/teams/localnative-bounty/issues):

[https://github.com/localnative/localnative-bounty/](https://github.com/localnative/localnative-bounty/)

GitHub is [bought](https://blogs.microsoft.com/blog/2018/06/04/microsoft-github-empowering-developers/) by Microsoft.
