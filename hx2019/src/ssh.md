# [Secure Shell](https://en.wikipedia.org/wiki/Secure_Shell)

## ssh key
- [ssh key generation](https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/)
- [add ssh key to GitLab](https://docs.gitlab.com/ee/ssh/README.html)

## VM
### host
`~/.ssh/config`
```
Host pureos
  HostName localhost
  Port 2222
  User USERNAME
  PreferredAuthentications publickey
  IdentityFile ~/.ssh/id_rsa
```

### guest
add your pub key to `~/.ssh/authorized_keys`

### port forwarding
host port 2222 to guest port 22
```
ssh pureos
```

## more

[scp](http://man7.org/linux/man-pages/man1/scp.1.html)

[rsync](http://man7.org/linux/man-pages/man1/rsync.1.html)
